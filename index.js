/*
CLient-server architecture
*/

let http = require("http");

// console.log(http);
/*
http module contains methods and other codes which allowed us to create server and let our client communicate through http
*/

http.createServer((req, res) => {
	//http://localhost:4000
	if(req.url === "/"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Hi! Welcome to our Homepage')

	//http://localhost:4000/login	
	} else if (req.url === "/login"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Welcome to the login page!')
	} else {
		res.writeHead(404, {'Content-Type':'text/plain'})
		res.end('Resource cannot be found')
	}
}).listen(4000);
console.log('Server is running on localhost:4000');

/*
createServer() - it creates server that handles requests and responses
- has an anonymous function that handles our client and our server response
parameters req/res respectively

anonymous function in the createServer method is able to receive two objects, req=request of the client, res=as the server response
each parameters are objects which contain details of a request and response as well as the method to handle them

res.writeHead() a method of the response object. This will allow us to add headers, which are additional information about our server's response

200 HTTP status (status codes)

res.end() - method of a response object which ends the server's response and sends a message as a string

*/
/*
.listen() assigns a port to a createServer

http://localhost:4000
localhost current machine
4000 port number assigned to where the process/server is listening/running

to run server, 
npm install -g nodemon >> nodemon <filename>

or

node <filename.js>


*/


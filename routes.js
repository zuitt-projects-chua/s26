const http = require("http");
const port = 4000;
const server = http.createServer((req,res) => {
	if(req.url === "/greeting"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
	   	res.end('Hello again')
	}else if(req.url === "/homepage"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
	   	res.end('This is the homepage!')
	} else{
		res.writeHead(404, {'Content-Type': 'text/plain'})
	   	res.end('Error 404: Page not Available')
	}
});



server.listen(port);
console.log(`Server is now accessible at localhost: ${port}`)



